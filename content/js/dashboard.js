/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9152276425464085, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.5485941320293398, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999142391584003, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.9462374723968066, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.7480703259005146, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.0511968085106383, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.8409291384886798, 500, 1500, "me"], "isController": false}, {"data": [0.8320976212542478, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.6363098758131284, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.9382610350076104, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.3138651471984805, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 60226, 0, 0.0, 248.35987779364183, 9, 3530, 49.0, 786.9000000000015, 1173.0, 1656.0, 200.24670915916064, 855.9303750573548, 235.43683504171943], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 1636, 0, 0.0, 914.9260391198043, 221, 2074, 932.0, 1292.0, 1390.0, 1596.5199999999995, 5.4474500872391145, 3.4418947328551828, 6.740155527863241], "isController": false}, {"data": ["getLatestMobileVersion", 34981, 0, 0.0, 42.46651039135537, 9, 1304, 26.0, 77.0, 121.0, 201.9900000000016, 116.64104727195127, 78.0264818176627, 85.99999090851875], "isController": false}, {"data": ["findAllConfigByCategory", 5887, 0, 0.0, 253.9140479021569, 33, 1332, 197.0, 520.1999999999998, 653.5999999999995, 895.4799999999996, 19.609214698750236, 22.175264278469502, 25.468999559900205], "isController": false}, {"data": ["getNotifications", 2332, 0, 0.0, 642.1710977701526, 78, 1635, 500.5, 1206.0, 1297.35, 1433.3400000000001, 7.762698436475362, 64.53501151339331, 8.634485858540467], "isController": false}, {"data": ["getHomefeed", 752, 0, 0.0, 1993.905585106384, 860, 3530, 1887.5, 2654.7000000000007, 2853.4, 3189.4700000000003, 2.5003574314147303, 30.465780929454674, 12.511554178290114], "isController": false}, {"data": ["me", 3401, 0, 0.0, 440.0185239635402, 70, 1381, 336.0, 887.8000000000002, 1012.8999999999996, 1197.92, 11.321910443388782, 14.865238269663537, 35.878515028121676], "isController": false}, {"data": ["findAllChildrenByParent", 3237, 0, 0.0, 462.33765832560965, 55, 1467, 328.0, 999.0, 1117.0, 1262.9599999999991, 10.778072266691527, 12.104280377632087, 17.219654519831387], "isController": false}, {"data": ["getAllClassInfo", 1691, 0, 0.0, 885.8095801300994, 90, 2044, 1023.0, 1384.8, 1440.7999999999997, 1639.5599999999995, 5.628467865143108, 16.150760834426187, 14.450431657676983], "isController": false}, {"data": ["findAllSchoolConfig", 5256, 0, 0.0, 284.41894977168937, 39, 1368, 229.0, 556.0, 708.0, 946.0, 17.51071931876106, 381.88416389313664, 12.842334187880423], "isController": false}, {"data": ["getChildCheckInCheckOut", 1053, 0, 0.0, 1422.3456790123455, 483, 2359, 1423.0, 1670.8000000000002, 1764.3, 1950.8400000000001, 3.504952867870267, 233.692047904143, 16.128259681059273], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 60226, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
